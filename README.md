# It's Been...
You finish the sentence.

For example, "it's been 2 days since our last accident."

This project is just an exercise in learning server side programming with Rust,
and maybe having some fun along the way.

The application is a web server that renders a page which displays the time since an epoch.
The epoch (both the instant and the label) are intended to be controlled by user input via
request query parameters.  An epoch is a timestamp, while a label is a human-readable string,
as a name for the epoch.  "Our last accident" is the name of an epoch.  The instant of such an
epoch  might be "2021-03-15T08:30:00+0000".  The page would then calculate and display the time
since that epoch.
