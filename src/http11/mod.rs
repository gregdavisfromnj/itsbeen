/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

use std::str::FromStr;
use std::collections::HashMap;

/**
 * An HTTP 1.1 header.  Key-value pair that is separated by a colon per
 * IETF RFC 7230.
 */
pub struct Header {
    pub key: String,
    pub value: String,
}

impl Header {
    pub fn from_string<'a>(text: &String) -> Vec<Header> {
        let mut headers: Vec<Header> = vec![];
        let pairs = text.lines();
        for pair in pairs {
            match pair.split_once(":") {
                Some((key, value)) => {
                    headers.push(Header { key: String::from(key.trim()), value: String::from(value.trim()) });
                }
                None => continue
            }
        }
        return headers;
    }
}

/**
 * A URI query string parameter token, as a key-value pair separated by a
 * equal symbol, per IETF RFC 3896.
 */
pub struct QueryParam {
    pub key: String,
    pub value: String,
}

impl QueryParam {
    fn from_string(text: &String) -> Result<Vec<QueryParam>, ()> {
        let mut params: Vec<QueryParam> = vec![];
        for pair in text.split('&') {
            match pair.split_once("=") {
                Some((key, value)) => {
                    params.push(QueryParam { key: String::from(key.trim()), value: String::from(value.trim()) });
                }
                None => continue
            }
        }
        Ok(params)
    }
}

/**
 * An HTTP 1.1 verb used to describe the action or method in a request
 * message.  This is at the beginning of the HTTP request start line per
 * IETF RFC 7230.
 */
#[derive(PartialEq, Eq, Debug)]
pub enum Verb {
    GET,
    POST,
    PUT,
    DELETE,
    ERROR,
}

/**
 * Implements the FromStr deserialization trait to convert a string to a Verb.
 * If the string cannot convert to a Verb, then a non-Verb value will be used
 * instead (`Verb::ERROR`).
 */
impl FromStr for Verb {
    type Err = ();

    fn from_str(text: &str) -> Result<Self, Self::Err> {
        match text.to_uppercase().as_str() {
            "GET" => { Ok(Verb::GET) }
            "POST" => { Ok(Verb::POST) }
            "PUT" => { Ok(Verb::PUT) }
            "DELETE" => { Ok(Verb::DELETE) }
            _ => { Err(()) }
        }
    }
}

/**
 * Implements the ToString serialization trait to convert a Verb to a string.
 * If the value of the Verb is `Verb::ERROR`, then a string with an invalid `ERROR`
 * method will be returned.
 */
impl ToString for Verb {
    fn to_string(&self) -> String {
        return match self {
            Verb::GET => { String::from("GET") }
            Verb::POST => { String::from("POST") }
            Verb::PUT => { String::from("PUT") }
            Verb::DELETE => { String::from("DELETE") }
            Verb::ERROR => { String::from("ERROR") }
        };
    }
}

/**
 * Implementations should evaluate the request and turn it into a `String`
 * containing a full HTTP 1.1 response message.
 */
pub trait RequestHandler {
    fn handle(request: &Request) -> Result<String, i16>;
}

type Handle = fn(request: &Request) -> Result<String, i16>;

#[derive(Debug)]
pub struct HttpError {
    pub status: u32,
    pub message: String,
}

pub struct Request {
    pub verb: Verb,
    pub path: String,
    pub query_params: Vec<QueryParam>,
    pub headers: Vec<Header>,
    pub body: String,
}

/**
 * Implements the FromStr deserialization trait to convert a string to a Verb.
 * If the string cannot convert to a Verb, then a non-Verb value will be used
 * instead (`Verb::ERROR`).
 */
impl FromStr for Request {
    type Err = HttpError;

    fn from_str(text: &str) -> Result<Self, Self::Err> {
        let mut request = Request {
            verb: Verb::GET,
            path: "".to_string(),
            query_params: vec![],
            headers: vec![],
            body: "".to_string()
        };
        let eol_length = 2;

        let mut request_text = String::from(text);

        let start_line_end = request_text.find("\r\n").unwrap_or(request_text.len());
        let start_line: String = request_text.drain(..start_line_end).collect();

        let mut start_line_splits = start_line.split(' ');
        match Verb::from_str(start_line_splits.next().unwrap_or_else(|| {"ERROR"})) {
            Ok(v) => request.verb = v,
            _ => return Err(HttpError{status: 405, message: String::from("Method not allowed")}),
        }

        let resource = start_line_splits.next().unwrap_or("ERROR");
        let mut resource_splits = resource.split('?');

        request.path = resource_splits.next().unwrap_or("ERROR").to_string();
        let query_string = resource_splits.next().unwrap_or("ERROR").to_string();
        match QueryParam::from_string(&query_string) {
            Ok(p) => request.query_params = p,
            _ => {}
        }

        let mut i = eol_length;
        while !request_text.is_empty() && i > 0 {
            request_text.drain(..1);
            i -= 1;
        }

        let headers_end = request_text.find("\r\n\r\n").unwrap_or(request_text.len());
        let raw_headers: String = request_text.drain(..headers_end).collect();

        request.headers = Header::from_string(&raw_headers);

        i = eol_length * 2;
        while !request_text.is_empty() && i > 0 {
            request_text.drain(..1);
            i -= 1;
        }

        let request_end = request_text.find("\0").unwrap_or(request_text.len());
        request.body = request_text.drain(..request_end).collect();

        return Ok(request);
    }
}


/**
 * This struct will route a full HTTP 1.1 request message in a `String`
 * to a `RequestHandler`, pre-loaded with the deconstructed request message metadata.
 */
pub struct RequestRouter {
    routes: HashMap<String, Handle>,
    error: Handle
}

impl RequestRouter {
    pub fn new() -> Self  {
        Self {
            routes: HashMap::new(),
            error: |_| -> Result<String, i16> {
                Ok(String::from("HTTP/1.1 400 Client Error\r\n\r\n<html><body><h1>Oops!</h1></body></html>"))
            }
        }
    }

    pub fn register(&mut self, route: String, handler: fn(&Request) -> Result<String, i16>) {
        self.routes.insert(route, handler);
    }

    pub fn register_error(&mut self, error_handler: fn(&Request) -> Result<String, i16>) {
        self.error = error_handler;
    }

    pub fn route(&mut self, request: &Request) -> Handle {
        return match self.routes.get(&request.path) {
            Some(&handle) => { handle },
            None => { self.error },
        }
    }

    pub fn route_error(&mut self, _request: &Request, _code: i16) -> Handle {
        // TODO use the request and code params to route and handle the error
        self.error
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    /****************************************************
     * Header Tests
     */

    #[test]
    fn header_deserializes_empty() {
        let request_result = Request::from_str("GET / HTTP/1.1\r\n\r\n");
        assert_eq!(request_result.is_err(), false);
        let request = request_result.unwrap();
        assert!(request.headers.is_empty());
    }


    #[test]
    fn header_deserializes_single() {
        let request_result = Request::from_str("GET / HTTP/1.1\r\nHost: localhost:8989\r\n\r\n");
        assert_eq!(request_result.is_err(), false);
        let request = request_result.unwrap();
        assert_eq!(request.headers.len(), 1);
        assert_eq!(request.headers[0].key, "Host");
        assert_eq!(request.headers[0].value, "localhost:8989");
    }

    #[test]
    fn header_deserializes_two() {
        let request_result = Request::from_str("GET / HTTP/1.1\r\nHost: localhost:8989\r\nAuthorization: Bearer blah_blah_blah\r\n\r\n");
        assert_eq!(request_result.is_err(), false);
        let request = request_result.unwrap();
        assert_eq!(request.headers.len(), 2);
        assert_eq!(request.headers[0].key, "Host");
        assert_eq!(request.headers[0].value, "localhost:8989");
        assert_eq!(request.headers[1].key, "Authorization");
        assert_eq!(request.headers[1].value, "Bearer blah_blah_blah");
    }

    #[test]
    fn header_deserializes_no_whitespace() {
        let request_result = Request::from_str("GET / HTTP/1.1\r\nHost: localhost:8989\r\nContent-Type:Awesomeness\r\n\r\n");
        assert_eq!(request_result.is_err(), false);
        let request = request_result.unwrap();
        assert_eq!(request.headers.len(), 2);
        assert_eq!(request.headers[1].key, "Content-Type");
        assert_eq!(request.headers[1].value, "Awesomeness");
    }

    #[test]
    fn header_deserializes_left_whitespace() {
        let request_result = Request::from_str("GET / HTTP/1.1\r\nHost: localhost:8989\r\nContent-Type:      Awesomeness\r\n\r\n");
        assert_eq!(request_result.is_err(), false);
        let request = request_result.unwrap();
        assert_eq!(request.headers.len(), 2);
        assert_eq!(request.headers[1].key, "Content-Type");
        assert_eq!(request.headers[1].value, "Awesomeness");
    }

    #[test]
    fn header_deserializes_right_whitespace() {
        let request_result = Request::from_str("GET / HTTP/1.1\r\nHost: localhost:8989\r\nContent-Type:Awesomeness          \r\n\r\n");
        assert_eq!(request_result.is_err(), false);
        let request = request_result.unwrap();
        assert_eq!(request.headers.len(), 2);
        assert_eq!(request.headers[1].key, "Content-Type");
        assert_eq!(request.headers[1].value, "Awesomeness");
    }

    #[test]
    fn header_deserializes_to_error_without_separator() {
        let request_result = Request::from_str("GET / HTTP/1.1\r\nHost: localhost:8989\r\nContent-Type Awesomeness\r\nAuthorization: Bearer blah_blah_blah\r\n\r\n");
        assert_eq!(request_result.is_err(), false);
        let request = request_result.unwrap();
        assert_eq!(request.headers.len(), 2);
        assert_eq!(request.headers[0].key, "Host");
        assert_eq!(request.headers[0].value, "localhost:8989");
        assert_eq!(request.headers[1].key, "Authorization");
        assert_eq!(request.headers[1].value, "Bearer blah_blah_blah");
    }


    /****************************************************
     * QueryParam Tests
     */

    #[test]
    fn queryparam_deserializes_empty() {
        let request_result = Request::from_str("GET / HTTP/1.1\r\n\r\n");
        assert_eq!(request_result.is_err(), false);
        let request = request_result.unwrap();
        assert!(request.query_params.is_empty());
    }

    #[test]
    fn queryparam_deserializes_single() {
        let request_result = Request::from_str("GET /?one=eh HTTP/1.1\r\nHost: localhost:8989\r\n\r\n");
        assert_eq!(request_result.is_err(), false);
        let request = request_result.unwrap();
        assert_eq!(request.query_params.len(), 1);
        assert_eq!(request.query_params[0].key, "one");
        assert_eq!(request.query_params[0].value, "eh");
    }

    #[test]
    fn queryparam_deserializes_two() {
        let request_result = Request::from_str("GET /?one=eh&two=bee HTTP/1.1\r\nHost: localhost:8989\r\n\r\n");
        assert_eq!(request_result.is_err(), false);
        let request = request_result.unwrap();
        assert_eq!(request.query_params.len(), 2);
        assert_eq!(request.query_params[0].key, "one");
        assert_eq!(request.query_params[0].value, "eh");
        assert_eq!(request.query_params[1].key, "two");
        assert_eq!(request.query_params[1].value, "bee");
    }

    #[test]
    fn queryparam_ignores_key_no_value() {
        let request_result = Request::from_str("GET /?one HTTP/1.1\r\nHost: localhost:8989\r\n\r\n");
        assert_eq!(request_result.is_err(), false);
        let request = request_result.unwrap();
        assert_eq!(request.query_params.len(), 0);
    }

    #[test]
    fn queryparam_deserializes_escaped() {
        let request_result = Request::from_str("GET /?one=%20eh&two=bee HTTP/1.1\r\nHost: localhost:8989\r\n\r\n");
        assert_eq!(request_result.is_err(), false);
        let request = request_result.unwrap();
        assert_eq!(request.query_params.len(), 2);
        assert_eq!(request.query_params[0].key, "one");
        assert_eq!(request.query_params[0].value, "%20eh");
        assert_eq!(request.query_params[1].key, "two");
        assert_eq!(request.query_params[1].value, "bee");
    }


    /****************************************************
     * Verb Tests
     */

    #[test]
    fn verb_deserializes_empty() {
        let request_result = Request::from_str("/ HTTP/1.1\r\n\r\n");
        assert_eq!(request_result.is_err(), true);
    }

    #[test]
    fn verb_deserializes_invalid() {
        let request_result = Request::from_str("FAIL /?one=eh HTTP/1.1\r\nHost: localhost:8989\r\n\r\n");
        assert_eq!(request_result.is_err(), true);
    }

    #[test]
    fn verb_deserializes_valid() {
        let request_result = Request::from_str("GET /?one=eh&two=bee HTTP/1.1\r\nHost: localhost:8989\r\n\r\n");
        assert_eq!(request_result.is_err(), false);
        let request = request_result.unwrap();
        assert_eq!(request.verb, Verb::GET);
    }

}