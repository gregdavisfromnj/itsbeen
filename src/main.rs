/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

mod controllers;

use std::str;
use std::str::FromStr;
use std::net::{TcpListener, TcpStream};
use std::io::{Read, Write, BufReader, BufWriter};
use std::sync::Arc;
use std::sync::atomic::{AtomicBool, Ordering};
use itsbeen::http11::{Request, RequestHandler, RequestRouter, Verb};
use crate::controllers::home::HomeRequestHandler;
use crate::controllers::error::ErrorHandler;

fn main() {
    let mut router = RequestRouter::new();

    router.register(String::from("/"), HomeRequestHandler::handle);
    router.register_error(ErrorHandler::handle);

    let running = Arc::new(AtomicBool::new(true));
    let r = running.clone();

    ctrlc::set_handler(move || {
        r.store(false, Ordering::SeqCst);
    }).expect("Error setting Ctrl-C handler");

    match TcpListener::bind("127.0.0.1:8989") {
        Ok(listener) => {
            while running.load(Ordering::SeqCst) {
                for stream in listener.incoming() {
                    match stream {
                        Ok(stream) => handle_connection(stream, &mut router),
                        Err(e) =>  println!("Problem accepting a connection! {}", e)
                    }
                }
            }
        },
        Err(e) => println!("Problem binding to a port! {}", e)
    }
}

/**
 * Handles an HTTP request.
 */
fn handle_connection(stream: TcpStream, router: &mut RequestRouter) {
    let buffered_reader = BufReader::new(&stream);
    let mut buffered_writer = BufWriter::new(&stream);
    let handled: Result<String, i16>;

    let mut raw_buffer = vec![0; 1024];
    let mut reader = buffered_reader.take(1024);
    let n_read = reader.read(&mut raw_buffer);


    if n_read.is_err() {
        println!("Error reading from the stream!");
        let bad_request = Request {
            verb: Verb::GET,
            path: "".to_string(),
            query_params: Vec::new(),
            headers: Vec::new(),
            body: "".to_string()
        };
        let error_handler = router.route_error(&bad_request, 400);
        handled = error_handler(&bad_request);
    } else {
        let utf8_buffer = str::from_utf8(&raw_buffer);
        let request = match utf8_buffer {
            Ok(buffer) => {
                let processed = match Request::from_str(buffer) {
                    Ok(r) => r,
                    Err(_) => {
                        let r = Request {
                            verb: Verb::GET,
                            path: "".to_string(),
                            query_params: Vec::new(),
                            headers: Vec::new(),
                            body: "".to_string()
                        };
                        r
                    }
                };
                processed
            },
            Err(_) => {
                let r = Request {
                    verb: Verb::GET,
                    path: "".to_string(),
                    query_params: Vec::new(),
                    headers: Vec::new(),
                    body: "".to_string()
                };
                r
            },
        };

        let handle_func = router.route(&request);
        handled = handle_func(&request);
    }
    buffered_writer.write(handled.unwrap().as_bytes()).unwrap();
}
