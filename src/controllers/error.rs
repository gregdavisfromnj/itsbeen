/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

use itsbeen::http11::*;

/**
 * Renders HTML for the error URL path ('/error').
 */
pub struct ErrorHandler {
}

impl RequestHandler for ErrorHandler {
    fn handle(request: &Request) -> Result<String, i16> {
        Ok(String::from("HTTP/1.1 400 OK\r\n\r\n<html><body><h1>Oops!</h1></body></html>"))
    }
}
